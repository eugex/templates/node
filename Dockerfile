FROM node:alpine

ENV PORT 3000
ENV HOME /usr/src/app/

WORKDIR $HOME
COPY . $HOME

RUN npm install --only=prod \
    && npm rebuild --quiet

EXPOSE $PORT

CMD [ "npm", "start" ]
