'use strict';

require('dotenv').config();

const app = require('../src/app');
const { should } = require('chai');

describe('App: ', () => {
  before(done => {
    app.init(err => {
      should().not.exist(err);

      done();
    });
  });
});
