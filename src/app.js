'use strict';

/**
 * @description Inicializa a aplicação
 * @param {Function} callback chamda de retorno `err => {};`
 */
exports.init = callback => {
  /* ... code ... */
  callback();
};
